var wlcangkus= [];
var isUpate = true; // 是否是修改，默认不是
var currentRow;
var search_data = {page:1,limit:10,
    cangku_type:stitle.value};

loadData();
function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>仓库类型名称</th><th>状态</th><th>创建时间</th><th>操作</th></tr>";
    for(var i=arr.length-1;i>=0;i--){
        var a = arr[i];
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.cangku_type+"</td>" +
            "<td class='ntitle'>"+a.ck_status+"</td>" +
            "<td class='time'>"+a.date.substring(0,10)+"</td>" +
            "<td class='ntitle'><a class='edit_a' onclick='upd(\""+a.i+"\")' href='javascript:;'>编辑</a>" +
            "<a class='del_a' onclick='del(\""+a.id+"\")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;
}

var search_data = {page:1,limit:10}
function search(){
    search_data = {page:1,limit:10,
        cangku_type:stitle.value,}
    loadData();
}


function loadData(){
    list_div.innerHTML = "";
    loading.style.display = "block";
    ajax("/find_all_wlcangku",search_data,function(data){
        wlcangkus = data;
        showData(wlcangkus);
        loading.style.display = "none";
    });
    ajax("/count_all_wlcangku",search_data,function(data){
        // console.log(data);  33
        data = parseInt(data);
        current_page.innerHTML = search_data.page;
        total_page.innerHTML =
            (data%search_data.limit==0?
                data/search_data.limit:
                Math.floor(data/search_data.limit)+1);
    });
}


function save(){
    if(!cangku_type.value){
        cangku_type_error.innerHTML = "请您填写仓库类型名称";
        return;
    }
    ajax(isUpate?"/upd_wlcangku":"/add_wlcangku",{
        id:currentRow?currentRow.id:-1,
        cangku_type:cangku_type.value,
        ck_status:ck_status.value
    },function(data){
        alert(isUpate?"更新成功！":"新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
        return_btn.style.display = "none";
        search_div.style.display = "block";
    })
}

function add(){
    isUpate = false;
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
    search_div.style.display = "none";
    return_btn.style.display = "block";

    cangku_type.value = "";
    ck_status.value = "";
    date.value = getNow();
    cangku_type.focus();
}


function del(id){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    ajax("/del_wlcangku", {id:id},function(data){
        alert("删除成功！");
        loadData();
    });
}


function upd(i){
    isUpate = true;
    var d = wlcangkus[i];
    currentRow = d;
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
    search_div.style.display = "none";
    return_btn.style.display = "block";

    cangku_type.value = d.cangku_type;
    ck_status.value = d.ck_status;
    date.value = d.date.substring(0,10);
    cangku_type.focus();
}






function getNow(){
    var now = new Date();
    var y = now.getFullYear();
    var m = now.getMonth()+1;
    var d = now.getDate();
    if(m<10) m = "0"+m;
    if(d<10) d = "0"+d;
    return y+"-"+m+"-"+d;
}


function returnMain(){
    add_div.style.display = "none";
    list_div.style.display = "block";
    save_btn.style.display = "none";
    add_btn.style.display = "block";
    search_div.style.display = "block";
    return_btn.style.display = "none";
}

function first_page(){
    search_data.page=1;
    loadData();
}

function pre_page(){
    if(search_data.page>1) {
        search_data.page--;
        loadData();
    }
}

function next_page(){
    if(search_data.page<parseInt(total_page.innerHTML)) {
        search_data.page++;
        loadData();
    }
}

function to_page(){
    var ip = parseInt(input_page.value);
    if(ip>=1&&ip<=parseInt(total_page.innerHTML)){
        search_data.page = ip;
        loadData();
    }
}

function end_page(){
    search_data.page = parseInt(total_page.innerHTML);
    loadData();
}