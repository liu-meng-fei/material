﻿var datas = [];
var isUpate = false; // 是否是修改，默认不是
var currentRow;
var search_data = {page:1,limit:10,
    name:stitle.value};
loadData();

loadZhanQu();
function loadZhanQu(){
    ajax("/find_all_zhanqu",{},function(data){
        
        for(var i=0;i<data.length;i++){
           var d = data[i];
            zhanqu_input.innerHTML += "<option value='"+d.id+"'>"+d.name+"</option>";
        }
    });
}
function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>门店名称</th><th>门店编码</th><th>负责人</th>" +
        "<th>负责人联系方式</th><th>所属战区</th><th>状态</th><th>操作</th></tr>";
    for(var i=arr.length-1;i>=0;i--){
        var a = arr[i];
        
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.name+"</td>" +
            "<td class='ntitle'>"+a.code+"</td>" +
            "<td class='ntitle'>"+a.admin_name+"</td>" +
            "<td class='ntitle'>"+a.admin_tel+"</td>" +
            "<td class='ntitle'>"+a.status+"</td>" +
            "<td class='ntitle'><a class='edit_a' onclick='upd("+i+")' href='javascript:;'>编辑</a>" +
            "<a class='del_a' onclick='del("+a.id+")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;
}

function search(){
    loadData({name:stitle.value,code:stitle.value});
}

function loadData(){
    list_div.innerHTML = "";
    loading.style.display = "block";
    ajax("/find_all_mendian",search_data,function(data){
        datas = data;
        showData(datas);
        loading.style.display = "none";
    });
    ajax("/count_all_mendian",search_data,function(data){
        //console.log(data);
        data = parseInt(data);
        current_page.innerHTML = search_data.page;
        total_page.innerHTML =
            (data%search_data.limit==0?
                data/search_data.limit:
                Math.floor(data/search_data.limit)+1);
    });
}
function first_page(){
    search_data.page=1;
    loadData();
}
function pre_page(){
    if(search_data.page>1) {
        search_data.page--;
        loadData();
    }
}
function next_page(){
    if(search_data.page<parseInt(total_page.innerHTML)) {
        search_data.page++;
        loadData();
    }
}
function to_page(){
    var ip = parseInt(input_page.value);
    if(ip>=1&&ip<=parseInt(total_page.innerHTML)){
        search_data.page = ip;
        loadData();
    }
}
function end_page(){
    search_data.page =parseInt(total_page.innerHTML);
    loadData();
}
function save(){
    if(!name.value){
        name_error.innerHTML = "请您填写门店名称";
        return;
    }
    ajax(isUpate?"/upd_mendian":"/add_mendian",{
        id:currentRow?currentRow.id:-1,
        name:m_name.value,
        code:mcode.value,
        admin_name:admin_name.value,
        admin_tel:admin_tel.value,
        status:status.value
    },function(data){
        alert(isUpate?"更新成功！":"新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
        return_btn.style.display = "none";
        search_div.style.display = "block";
    })
}
function del(id){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    ajax("/del_mendian", {id:id},function(data){
        alert("删除成功！");
        loadData();
    });
}
function upd(i){
    isUpate = true;
    var d = datas[i];
    currentRow = d;
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
    search_div.style.display = "none";
    return_btn.style.display = "block";

    name.value = d.mname;
    code.value = d.mcode;
    admin_name.value = d.admin_name;
    admin_tel.value = d.admin_tel;
    status.value = d.status;
    name.focus();
}
function add(){
    isUpate = false;
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
    search_div.style.display = "none";
    return_btn.style.display = "block";

    name.value = " ";
    code.value = "";
    admin_name.value = "";
    admin_tel.value = "";
    status.value = "";
    name.focus();
}

function returnMain(){
    add_div.style.display = "none";
    list_div.style.display = "block";
    save_btn.style.display = "none";
    add_btn.style.display = "block";
    search_div.style.display = "block";
    return_btn.style.display = "none";
}