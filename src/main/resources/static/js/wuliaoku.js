var wuliaokus= [];

var currentRow;
var isUpate = false; // 是否是修改，默认不是

var search_data = {page:1,limit:10,wl_name:stitle.value,wl_code:stitle.value};
loadData();

function search(){
    search_data = {page:1,limit:10,wl_name:stitle.value,wl_code:stitle.value}
    loadData();
}

function loadData(){
    list_div.innerHTML = "";
    loading.style.display = "block";
    ajax("/find_all_wuliaoku",search_data,function(data){
       wuliaokus = data;
        showData(wuliaokus);
        loading.style.display = "none";
    });
    ajax("/count_all_wuliaoku",search_data,function(data){
      //  console.log(data);
        data = parseInt(data);
        current_page.innerHTML = search_data.page;
        total_page.innerHTML =
            (data%search_data.limit==0?
                data/search_data.limit:
                Math.floor(data/search_data.limit)+1);
    });
}

function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>物料类型</th><th>物料名称</th><th>物料编码</th><th>物料SN号</th>" +
        "<th>供应商类型</th><th>供应商名称</th><th>供应商编码</th>"+"<th>物料仓库</th><th>成本价</th><th>状态</th><th>操作</th></tr>";
    for(var i = arr.length-1;i>=0;i--){
        var a = arr[i];
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.wl_style+"</td>" +
            "<td class='ntitle'>"+a.wl_name+"</td>" +
            "<td class='ntitle'>"+a.wl_code+"</td>" +
            "<td class='ntitle'>"+a.wl_SN+"</td>" +
            "<td class='ntitle'>"+a.gys_style+"</td>" +
            "<td class='ntitle'>"+a.gys_name+"</td>" +
            "<td class='ntitle'>"+a.gys_code+"</td>" +
            "<td class='ntitle'>"+a.wlck+"</td>" +
            "<td class='ntitle'>"+a.daijia+"</td>"
            + "<td class='ntitle'>"+a.zhuangtai+"</td>" +
            "<td class='ntitle'><a class='edit_a' onclick='upd(\""+i+"\")' href='javascript:;'>编辑</a>" +
            "<a class='del_a' onclick='del(\""+a.wl_name+"\")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;
}

function upd(i){
    isUpate = true;
    var d = wuliaokus[i];
    currentRow = d;
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
    search_div.style.display = "none";
    return_btn.style.display = "block";

    wl_style.value = d.wl_style;
    wl_name.value = d.wl_name;
    wl_code.value = d.wl_code;
    wl_SN.value = d.wl_SN;
    gys_style.value = d.gys_style;
    gys_name.value = d.gys_name;
    gys_code.value = d.gys_code;
    wlck.value = d.wlck;
    daijia.value = d.daijia;
    zhuangtai.value = d.zhuangtai;
    wl_name.focus();
}

function save(){
    if(!wl_name.value){
        wl_name_error.innerHTML = "请您填写物料名称";
      return;
    }
    ajax(isUpate?"/upd_wuliaoku":"/add_wuliaoku",{
        id:currentRow?currentRow.id:-1,
        wl_style:wl_style.value,
        wl_name: wl_name.value,
        wl_code: wl_code.value,
        wl_SN: wl_SN.value,
        gys_style:gys_style.value,
        gys_name: gys_name.value,
        gys_code: gys_code.value,
        wlck:wlck.value,
        daijia:daijia.value,
        zhuangtai:zhuangtai.value},function(data){
        alert(isUpate?"更新成功！":"新增成功！");
        loadData();
                add_div.style.display = "none";
                list_div.style.display = "block";
                save_btn.style.display = "none";
                add_btn.style.display = "block";
                return_btn.style.display = "none";
                search_div.style.display = "block";
    })
}

function del(wl_name){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    ajax("/del_wuliaoku", {wl_name:wl_name},function(data){
       alert("删除成功！");
       loadData();
    });
}

function returnMain(){
    add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
        search_div.style.display = "block";
        return_btn.style.display = "none";
}

function add(){
    isUpate = false;
        add_div.style.display = "block";
        list_div.style.display = "none";
        save_btn.style.display = "block";
        add_btn.style.display = "none";
        search_div.style.display = "none";
        return_btn.style.display = "block";

     wl_style.value ="";
        wl_name.value = "";
        wl_code.value ="";
        wl_SN.value = "";
        gys_style.value = "";
        gys_name.value = "";
        gys_code.value ="";
        wlck.value = "";
        daijia.value = "";
        zhuangtai.value = "";
        wl_name.focus();
}

function first_page(){
    search_data.page=1;
    loadData();
}

function pre_page(){
    if(search_data.page>1) {
        search_data.page--;
        loadData();
    }
}

function next_page(){
    if(search_data.page<parseInt(total_page.innerHTML)) {
        search_data.page++;
        loadData();
    }
}

function to_page(){
    var ip = parseInt(input_page.value);
    if(ip>=1&&ip<=parseInt(total_page.innerHTML)){
        search_data.page = ip;
        loadData();
    }
}

function end_page(){
        search_data.page =parseInt(total_page.innerHTML);
        loadData();
}
