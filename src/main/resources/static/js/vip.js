var vips = [];
var isUpate = false; // 是否是修改，默认不是
var currentRow;
var search_data = {page:1,limit:10,
    type:stitle.value,
    status:stitle.value
   };

    loadData();
function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>套餐类型</th><th>时间</th>" +
        "<th>状态</th><th>操作</th></tr>";
    for(var i=arr.length-1;i>=0;i--){
        var a = arr[i];
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.type+"</td>" +
            "<td class='ntitle'>"+a.time.substring(0,10)+"</td>" +
            "<td class='ntitle'>"+a.status+"</td>" +
            "<td class='ntitle'><a class='edit_a' onclick='upd("+i+")' href='javascript:;'>编辑</a>" +
            "<a class='del_a' onclick='del("+a.id+")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;
}

function search(){
    search_data = {page:1,limit:10,
        type:stitle.value,
        status:stitle.value
       }
    loadData();
}

function loadData(){
    list_div.innerHTML = "";
    loading.style.display = "block";
    ajax("/find_all_vip",search_data,function(data){
         vips = data;
         showData(vips);
         loading.style.display = "none";
    });

    ajax("/count_all_vip",search_data,function(data){
       // console.log(data);  33
        data = parseInt(data);
        current_page.innerHTML = search_data.page;
        total_page.innerHTML =
            (data%search_data.limit==0?
                data/search_data.limit:
                Math.floor(data/search_data.limit)+1);
    });
}
function next_page(){
    if(search_data.page<parseInt(total_page.innerHTML)) {
        search_data.page++;
        loadData();
    }
}
function end_page(){
    
        search_data.page=parseInt(total_page.innerHTML);
        loadData();
    
}
function first_page(){
   
        search_data.page=1;
        loadData();
    }
    function last_page(){
    
        if(search_data.page>1){
        search_data.page--;
        loadData();
        }
}  
function to_page(){
    var ip = parseInt(input_page.value);
    if(ip>=1&&ip<=parseInt(total_page.innerHTML)){
        search_data.page = ip;
        loadData();
    }
}


function save(){
    if(!type.value){
        type_error.innerHTML = "请您填写会员类型！";
        return;
}
  ajax(isUpate?"/upd_vip":"/add_vip",{
    id:currentRow?currentRow.id:-1,
    type:type.value,
    status:status1.value
},function(data){
    alert(isUpate?"更新成功！":"新增成功！");
    loadData();
    add_div.style.display = "none";
    list_div.style.display = "block";
    save_btn.style.display = "none";
    add_btn.style.display = "block";
    return_btn.style.display = "none";
    search_div.style.display = "block";
})

}
function del(id){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    var req = new XMLHttpRequest();
    req.open("post","/del_vip");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    var data = {
        id:id
    }
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            if(req.responseText.trim()=="1"){
                alert("删除成功！");
            }

            loadData();
        }
    }
}

function upd(i){
    isUpate = true;
    var d = vips[i];
    currentRow = d;
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
    search_div.style.display = "none";
    return_btn.style.display = "block";

   type.value = d.type;

    time.value = d.time.substring(0,10);
    type.focus();
}
function add(){
    isUpate = false;
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
    search_div.style.display = "none";
    return_btn.style.display = "block";

    type.value = "";
    //time.value = getNow();
    type.focus();
}
function getNow(){
    var now = new Date();
    var y = now.getFullYear();
    var m = now.getMonth()+1;
    var d = now.getDate();
    if(m<10) m = "0"+m;
    if(d<10) d = "0"+d;
    return y+"-"+m+"-"+d;
}

function returnMain(){
    add_div.style.display = "none";
    list_div.style.display = "block";
    save_btn.style.display = "none";
    add_btn.style.display = "block";
    search_div.style.display = "block";
    return_btn.style.display = "none";
}