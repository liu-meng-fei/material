var menus = [];
var isUpate = false; // 是否是修改，默认不是
var currentRow;
var search_data = {page:1,limit:10,
    menu_name:stitle.value,
    url:stitle.value};

loadData();
function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>菜单名称</th><th>菜单网址</th><th>操作</th></tr>";
    for(var i=0;i<menus.length;i++){
        var a = arr[i];
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.menu_name+"</td>" +
            "<td class='ntitle'>"+a.url+"</td>" +
            "<td class='ntitle'><a class='edit_a' onclick='upd("+i+")' href='javascript:;'>编辑</a>" +
            "<a class='del_a' onclick='del("+a.id+")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;
}
function search(){
    search_data = {page:1,limit:10,
        menu_name:stitle.value}
    loadData();
}

function loadData(){
    list_div.innerHTML = "";
    loading.style.display = "block";
    ajax("/find_all_menu",search_data,function(data){
        menus = data;
        showData(menus);
        loading.style.display = "none";
    });
    ajax("/count_all_menu",search_data,function(data){
        //console.log(data);
        data = parseInt(data);
        current_page.innerHTML = search_data.page;
        total_page.innerHTML =
            (data%search_data.limit==0?
                data/search_data.limit:
                Math.floor(data/search_data.limit)+1);
    });
}
function first_page(){
    search_data.page=1;
    loadData();
}
function pre_page(){
    if(search_data.page>1) {
        search_data.page--;
        loadData();
    }
}
function next_page(){
    if(search_data.page<parseInt(total_page.innerHTML)) {
        search_data.page++;
        loadData();
    }
}
function to_page(){
    var ip = parseInt(input_page.value);
    if(ip>=1&&ip<=parseInt(total_page.innerHTML)){
        search_data.page = ip;
        loadData();
    }
}
function end_page(){
    search_data.page =parseInt(total_page.innerHTML);
    loadData();
}
function save(){
    if(!menu_name.value){
        menu_name_error.innerHTML = "请您填写超期物料名称";
        return;
    }
    ajax(isUpate?"/upd_menu":"/add_menu",{
        id:currentRow?currentRow.id:-1,
        menu_name:menu_name.value,
        url:url.value,
    },function(data){
        alert(isUpate?"更新成功！":"新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
        return_btn.style.display = "none";
        search_div.style.display = "block";
    })
}
function del(id){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    ajax("/del_menu", {id:id},function(data){
        alert("删除成功！");
        loadData();
    });
}
function upd(i){
    isUpate = true;
    var d = menus[i];
    currentRow = d;
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
    search_div.style.display = "none";
    return_btn.style.display = "block";

    menu_name.value = d.menu_name;
    url.value = d.url;
    menu_name.focus();
}
function add(){
    isUpate = false;
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
    search_div.style.display = "none";
    return_btn.style.display = "block";

    menu_name.value = "";
    url.value = "";
}

function returnMain(){
    add_div.style.display = "none";
    list_div.style.display = "block";
    save_btn.style.display = "none";
    add_btn.style.display = "block";
    search_div.style.display = "block";
    return_btn.style.display = "none";
}