package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface VipDao {
    List<Map> find_all_vip(Map map);
    long count_all_vip(Map map);
    int add_vip(Map map);
    int del_vip(Map map);
    int upd_vip(Map map);
}
