package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface MenuDao {
    List<Map> find_all_menu(Map map);
    long count_all_menu(Map map);
    int add_menu(Map map);
    int del_menu(Map map);
    int upd_menu(Map map);
}
