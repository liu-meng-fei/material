package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface MenDianDao {
    List<Map> find_all_mendian(Map map);
    long count_all_mendian(Map map);
    int add_mendian(Map map);
    int del_mendian(Map map);
    int upd_mendian(Map map);
}
