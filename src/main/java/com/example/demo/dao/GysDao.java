package com.example.demo.dao;


import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface GysDao {
    long count_all_lutao(Map map);

    int upd_gys(Map map);

    int del_gys(Map map);

    int add_gys(Map map);

    List<Map> find_all_gys(Map map);
}