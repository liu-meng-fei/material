package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface WlcqDao {
    List<Map> find_all_wlcq(Map map);
    long count_all_wlcq(Map map);
    int add_wlcq(Map map);
    int del_wlcq(Map map);
    int upd_wlcq(Map map);

}