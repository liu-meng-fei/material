package com.example.demo.controller;

import com.example.demo.dao.NoticeDao;
import com.example.demo.dao.ZhanQuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class NoticeController {
    @Autowired
    NoticeDao noticeDao;
    @PostMapping("/count_all_notice")
    public long count_all_notice(@RequestBody Map map){
        return noticeDao.count_all_notice(map);
    }
    @PostMapping("/del_notice")
    public int del_notice(@RequestBody Map map){
        return noticeDao.del_notice(map);
    }
    @PostMapping("/upd_notice")
    public int upd_notice(@RequestBody Map map){
        return noticeDao.upd_notice(map);
    }
    @PostMapping("/add_notice")
    public int add_notice(@RequestBody Map map){
        return noticeDao.add_notice(map);
    }
    @PostMapping("/find_all_notice")
    public List<Map> find_all_notice(@RequestBody Map map) throws InterruptedException {
        return noticeDao.find_all_notice(map);
    }
}
