package com.example.demo.controller;

import com.example.demo.dao.MenDianDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class MenDianController {
    @Autowired
    MenDianDao menDianDao;

    @PostMapping("/del_mendian")
    public int del_mendian(@RequestBody Map map){
        return menDianDao.del_mendian(map);
    }

    @PostMapping("/upd_mendian")
    public int upd_mendian(@RequestBody Map map){
        return menDianDao.upd_mendian(map);
    }

    @PostMapping("/add_mendian")
    public int add_mendian(@RequestBody Map map){
        return menDianDao.add_mendian(map);
    }

    @PostMapping("/upd_mendian")
    public int upd_MenDian(@RequestBody Map map){
        return menDianDao.upd_mendian(map);
    }
    @PostMapping("/find_all_mendian")
    public List<Map> find_all_mendian(@RequestBody Map map) throws InterruptedException {
        Thread.sleep(200);
        return menDianDao.find_all_mendian(map);
    }
    @PostMapping("/count_all_mendian")
    public long count_all_mendian(@RequestBody Map map) throws InterruptedException {
        Thread.sleep(200);
        return menDianDao.count_all_mendian(map);
    }
}