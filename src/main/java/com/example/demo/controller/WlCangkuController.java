package com.example.demo.controller;

import com.example.demo.dao.NoticeDao;
import com.example.demo.dao.WlCangkuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class WlCangkuController {
    @Autowired
    WlCangkuDao wlCangkuDao;

    @PostMapping("/upd_wlcangku")
    public int upd_wlcq(@RequestBody Map map){
        return wlCangkuDao.upd_wlcangku(map);
    }

    @PostMapping("/del_wlcangku")
    public int del_wlcangku(@RequestBody Map map){
        return wlCangkuDao.del_wlcangku(map);
    }

    @PostMapping("/add_wlcangku")
    public int add_wlcangku(@RequestBody Map map){
        return wlCangkuDao.add_wlcangku(map);
    }

    @PostMapping("/find_all_wlcangku")
    public List<Map> find_all_wlcangku(@RequestBody Map map) throws InterruptedException {
        return wlCangkuDao.find_all_wlcangku(map);
    }
    @PostMapping("/count_all_wlcangku")
    public long count_all_wlcangku(@RequestBody Map map) throws InterruptedException {
        Thread.sleep(200);
        return wlCangkuDao.count_all_wlcangku(map);
    }
}
