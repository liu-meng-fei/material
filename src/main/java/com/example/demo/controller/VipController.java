package com.example.demo.controller;

import com.example.demo.dao.VipDao;
import com.example.demo.dao.ZhanQuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class VipController {
    @Autowired
    VipDao vipDao;
    @PostMapping("/count_all_vip")
    public long count_all_vip(@RequestBody Map map){
        return vipDao.count_all_vip(map);
    }
    @PostMapping("/del_vip")
    public int del_vip(@RequestBody Map map){
        return vipDao.del_vip(map);
    }
    @PostMapping("/upd_vip")
    public int upd_vip(@RequestBody Map map){
        return vipDao.upd_vip(map);
    }
    @PostMapping("/add_vip")
    public int add_vip(@RequestBody Map map){
        return vipDao.add_vip(map);
    }
    @PostMapping("/find_all_vip")
    public List<Map> find_all_vip(@RequestBody Map map) throws InterruptedException {
        return vipDao.find_all_vip(map);
    }
}
