package com.example.demo.controller;

import com.example.demo.dao.MenuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class MenuController {
    @Autowired
    MenuDao menuDao;

    @PostMapping("/del_menu")
    public int del_menu(@RequestBody Map map){
        return menuDao.del_menu(map);
    }

    @PostMapping("/upd_menu")
    public int upd_menu(@RequestBody Map map){
        return menuDao.upd_menu(map);
    }

    @PostMapping("/add_menu")
    public int add_menu(@RequestBody Map map){
        return menuDao.add_menu(map);
    }

    @PostMapping("/find_all_menu")
    public List<Map> find_all_menu(@RequestBody Map map) throws InterruptedException {
        Thread.sleep(200);
        return menuDao.find_all_menu(map);
    }
    @PostMapping("/count_all_menu")
    public long count_all_menu(@RequestBody Map map) throws InterruptedException {
        Thread.sleep(200);
        return menuDao.count_all_menu(map);
    }
}
