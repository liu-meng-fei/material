package com.example.demo.controller;

import com.example.demo.dao.ZhiFuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class ZhiFuController {
    @Autowired
    ZhiFuDao zhiFuDao;
    @PostMapping("/count_all_zhifu")
    public long count_all_zhifu(@RequestBody Map map){
        return zhiFuDao.count_all_zhifu(map);
    }

    @PostMapping("/del_zhifu")
    public int del_zhifu(@RequestBody Map map){
        return zhiFuDao.del_zhifu(map);
    }
    
    @PostMapping("/upd_zhifu")
    public int upd_zhifu(@RequestBody Map map){
        return zhiFuDao.upd_zhifu(map);
    }

    @PostMapping("/add_zhifu")
    public int add_zhifu(@RequestBody Map map){
        return zhiFuDao.add_zhifu(map);
    }

    @PostMapping("/find_all_zhifu")
    public List<Map> find_all_zhifu(@RequestBody Map map) throws InterruptedException {

        return zhiFuDao.find_all_zhifu(map);
    }
}
