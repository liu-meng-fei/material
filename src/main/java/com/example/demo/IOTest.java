package com.example.demo;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class IOTest {

    public static void main(String[] args) throws BadHanyuPinyinOutputFormatCombination, IOException {
        String str = "物料类型";
        HanyuPinyinOutputFormat format =
                new HanyuPinyinOutputFormat();
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        String ss = PinyinHelper.toHanyuPinyinString(str,
                format,"");
        generateDao(ss);
    }

    public static void generateDao(String ss)
            throws IOException {
        String classNameLower = ss;
        String classNameUpper = ss.substring(0,1).toUpperCase()
                +ss.substring(1);
        FileWriter fw = new FileWriter
                ("/Users/zhengzhang/desktop/demo/src/main/java/com/example/demo/dao/" +
                        ""+classNameUpper+"Dao.java");
        fw.write("package com.example.demo.dao;\n" +
                "\n" +
                "import java.util.List;\n" +
                "import java.util.Map;\n" +
                "\n" +
                "public interface "+classNameUpper+"Dao {\n" +
                "    List<Map> find_all_"+classNameLower+"(Map map);\n" +
                "    long count_all_"+classNameLower+"(Map map);\n" +
                "    int add_"+classNameLower+"(Map map);\n" +
                "    int del_"+classNameLower+"(Map map);\n" +
                "    int upd_"+classNameLower+"(Map map);\n" +
                "}\n");
        fw.close();
    }
}
